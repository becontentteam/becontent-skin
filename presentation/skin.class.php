<?php

namespace becontent\skin\presentation;

use becontent\core\control\Settings as Settings;
use becontent\beContent as beContent;

class Skin {
	
	/**
	 *
	 * @var unknown
	 */
	protected $name;
	/**
	 *
	 * @var unknown
	 */
	protected $frame;
	/**
	 *
	 * @var unknown
	 */
	protected $private;
	/**
	 *
	 * @var unknown
	 */
	protected $smarty;
	/**
	 *
	 * @var unknown
	 */
	protected $head;
	/**
	 *
	 * @var unknown
	 */
	protected $appRoot;
	
	/**
	 *
	 * @var unknown
	 */
	protected $skinPath;
	
	/**
	 *
	 * @param string $skin        	
	 */
	function __construct($skin = "") {
		if ($skin == "") {
			$skin = Settings::getSkinName ();
		} else {
			Settings::setSkinName ( $skin );
		}
		$this->name = Settings::getSkin ();
		$this->appRoot = "/";
		$this->smarty = new \Smarty ();
		$this->smarty->setTemplateDir ( Settings::getSkin () );
		$this->smarty->setCompileDir ( Settings::getCompiledSkins () );
		$this->appRoot = Settings::getActualAppRoot ();
	}
	
	/**
	 */
	function resolve() {
		$this->skinPath = Settings::getSkin ();
		
		if (class_exists ( "Auth" )) {
			if (isset ( $this->frame )) {
			/**
			 * Do nothing
			 */
			} else {
				$this->head = new Skinlet ( "frame-private-head.html" );
				$this->frame = "frame-private";
			}
			$this->private = true;
		} else {
			if (isset ( $this->frame )) {
			/**
			 * Do nothing
			 */
			} else {
				$this->head = new Skinlet ( "frame-public-head.html" );
				$this->frame = "frame-public";
			}
			$this->private = false;
		}
		
		$this->template_name = $this->skinPath . "/{$this->frame}.html";
		
		if (! file_exists ( $this->template_name )) {
			$this->skinPath = Settings::getFallbackSkinsPath ();
			$this->template_name = $this->skinPath . "/{$this->frame}.html";
		}
	}
	/**
	 *
	 * @param unknown $frame        	
	 */
	function setFrame($frame) {
		$this->frame = $frame;
	}
	
	/**
	 *
	 * @param unknown $appRoot        	
	 */
	function setAppRoot($appRoot) {
		$this->appRoot = $appRoot;
	}
	
	/**
	 */
	function close() {
		$this->resolve ();
		
		$this->init ();
		
		$this->setContent ( "sys", beContent::getInstance () );
		
		if (is_string ( $value )) {
			$value = str_replace ( '\\', '/', $value );
		}
		
		$this->setContent ( "skinPath", str_replace ( '\\', '/', $this->skinPath . '/../..' ) );
		
		$this->setContent ( "appRoot", str_replace ( '\\', '/', $this->appRoot ) );
		
		$this->smarty->display ( $this->template_name );
	}
	
	/**
	 *
	 * @param unknown $name        	
	 * @param unknown $value        	
	 * @param string $pars        	
	 */
	function setContent($name, $value, $pars = "") {
		$this->smarty->assign ( $name, $value );
	}
	
	/**
	 */
	function get() {
		return $this->smarty->fetch ( $this->template_name );
	}
	
	/**
	 */
	function init() {
		if (! isset ( $this->frame ) || $this->frame == "frame-public.html" || $this->frame == "frame-private.html") {
			$this->setContent ( "head", $this->head->get () );
			$header = new Skinlet ( "header" );
			$this->setContent ( "header", $header->get () );
			$footer = new Skinlet ( "footer" );
			$this->setContent ( "footer", $footer->get () );
		}
	}
}
?>