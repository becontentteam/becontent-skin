<?php

namespace becontent\skin\presentation;

use becontent\core\control\Settings as Settings;
use becontent\beContent as beContent;

class Skinlet extends Skin {
	protected $smarty, $template_name, $skinletPath;
	
	function __construct($template) {
		
		$this->appRoot = Settings::getActualAppRoot ();
		
		if (! strpos ( $template, "." )) {
			$this->template_name = $template . ".html";
		} else {
			$this->template_name = $template;
		}
		
		if (file_exists ( Settings::getSkin () . "/{$this->template_name}" )) {
			$this->skinletPath = Settings::getSkin () . "/{$this->template_name}";
			$this->skinPath=Settings::getSkin ();
		} else {
			$this->skinletPath = Settings::getFallBackSkinsPath () . "/{$this->template_name}";
			$this->skinPath= Settings::getFallBackSkinsPath ();
		}
		
		$this->smarty = new \Smarty ();
		
		$this->smarty->setTemplateDir ( Settings::getSkin () );
		$this->smarty->setCompileDir ( Settings::getCompiledSkins () );
		
	}
	
	
	function setContent($name, $value, $pars = "") {
		$this->smarty->assign ( $name, $value );
	}
	
	
	function get() {
		$turnback = "";
		$this->smarty->assign ( "sys", beContent::getInstance () );
		$this->smarty->assign ( "skinPath", $this->skinPath . '/../..' );
		$this->smarty->assign ( "appRoot", $this->appRoot );
		
		if (file_exists ( Settings::getSkin () . "/{$this->template_name}" )) {
			$this->skinletPath = Settings::getSkin () . "/{$this->template_name}";
			$turnback = $this->smarty->fetch ( Settings::getSkin () . "/{$this->template_name}" );
		} else {
			$this->skinletPath = Settings::getFallBackSkinsPath () . "/{$this->template_name}";
			$turnback = $this->smarty->fetch ( Settings::getFallBackSkinsPath () . "/{$this->template_name}" );
		}
		return $turnback;
	}
}

?>